
import numpy
import cv2

#init face detector
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#init eye detector
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

#load image
img = cv2.imread('family.jpg')
#rgb -> gray
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#face detection
faces = face_cascade.detectMultiScale(gray, 1.3, 5)

for (x,y,w,h) in faces:

    roi_gray = gray[y:y+h, x:x+w]
    roi_color = img[y:y+h, x:x+w]
    
    # face rect    
    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

    # eyes detection
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex,ey,ew,eh) in eyes:
        #eye rect
        cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

#show image with face and eye rects
cv2.imshow('img',img)
#pause
cv2.waitKey(0)
#end
cv2.destroyAllWindows()
